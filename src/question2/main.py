#!/bin/env python3

import sys
import re

# Print to the error standard output how to correctly execute the script
def usage():
    print("Usage:\n\
    \t$> ./main.py [INPUT_FILE]\n\
    or:\n\
    \t$> python main.py [INPUT_FILE]", file=sys.stderr)
    exit(1)

# Check if the received line corresponds to a name line and parse it if it does
def popular_firstname(line):
    # Check if it a name line and if it does check if the full_name is correctly fomated
    if line.find("--") > 0 and popular_firstname.regex.match(line):
        full_name = popular_firstname.regex.match(line).group()
        first_name = full_name.split(", ")[1]
        # Check if there already are a cell in the dictionnary corresponding to the first_name:
        #   - If not, the cell is initialized
        #   - If true the corresponding value is incremented
        if first_name not in popular_firstname.first_name_nbr:
            popular_firstname.first_name_nbr[first_name] = 1
        else:
            popular_firstname.first_name_nbr[first_name] += 1

def search_popular_firstname(file_name):
    # Regular expression used to check if names are correctly formated
    popular_firstname.regex = re.compile("[a-zA-Z]+, [a-zA-Z]+")
    popular_firstname.first_name_nbr = {}
    try:
        # Open and read line by line the file
        with open(file_name, 'r') as fd:
            for line in fd:
                popular_firstname(line)
    except IOError:
        print("Cannot open the file: " + file_name, file=sys.stderr)
        usage()
    else:
        fd.close()
        count = 0
        print("The ten most common first names are:")
        for name in sorted(popular_firstname.first_name_nbr, key=popular_firstname.first_name_nbr.get, reverse=True):
            if count == 10:
                break
            print("  " + name, '(' + str(popular_firstname.first_name_nbr[name]) + ')')
            count += 1

def main():
    # Check if parameters seams ok
    if len(sys.argv) != 2:
        usage()
    search_popular_firstname(sys.argv[1])

if __name__ == "__main__":
    main()