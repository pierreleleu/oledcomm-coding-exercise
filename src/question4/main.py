#!/bin/env python3

import sys
import re

# Print on the error standard output how to correctly execute the script
def usage():
    print("Usage:\n\
    \t$> ./main.py [INPUT_FILE]\n\
    or:\n\
    \t$> python main.py [INPUT_FILE]", file=sys.stderr)
    exit(1)

# Loop on found lastname and firstname and check each combination to see if it's already exist
# if it didn't exist, the name is added to the list of new names
def create_new_unique_name():
    new_unique_names = []

    for first_name in unique_name.first_name:
        for last_name in unique_name.last_name:
            if len(new_unique_names) == 25:
                break
            potential_new_name = [last_name, first_name]
            if potential_new_name not in unique_name.full_name:
                new_unique_names.append(potential_new_name)
        if len(new_unique_names) == 25:
            break
    return new_unique_names

# Check if the received line corresponds to a name line and parse it if it does,
# Check if nither the lastname nor the fistname have been seen previously and add the unique name to the list
def unique_name(line):
    # Check if it a name line and if it does check if the full_name is correctly fomated
    if line.find("--") > 0 and unique_name.regex.match(line):
        full_name = unique_name.regex.match(line).group().split(", ")
        if full_name[0] not in unique_name.last_name and full_name[1] not in unique_name.first_name:
            unique_name.last_name.append(full_name[0])
            unique_name.first_name.append(full_name[1])
            unique_name.full_name.append(full_name)


def search_unique_name(file_name):
    # Regular expression used to check if names are correctly formated
    unique_name.regex = re.compile("[a-zA-Z]+, [a-zA-Z]+")
    unique_name.first_name = []
    unique_name.last_name = []
    unique_name.full_name = []

    try:
        # Open and read line by line the file
        with open(file_name, 'r') as fd:
            for line in fd:
                if len(unique_name.full_name) == 25:
                    break
                unique_name(line)
            new_unique_names = create_new_unique_name()
    except IOError:
        print("Cannot open the file: " + file_name, file=sys.stderr)
        usage()
    else:
        fd.close()
        print("The new 25 unique names are:")
        for new_unique_name in new_unique_names:
            print('  ' + new_unique_name[0] + ', ' + new_unique_name[1])

def main():
    # Check if parameters seams ok
    if len(sys.argv) != 2:
        usage()
    search_unique_name(sys.argv[1])

if __name__ == "__main__":
    main()