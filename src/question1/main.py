#!/bin/env python3

import sys
import re

# Print to the error standard output how to correctly execute the script
def usage():
    print("Usage:\n\
    \t$> ./main.py [INPUT_FILE]\n\
    or:\n\
    \t$> python main.py [INPUT_FILE]", file=sys.stderr)
    exit(1)

# Check if the received line corresponds to a name line and parse it if it does
def unique_names(line):
    # Check if it a name line and if it does check if the full_name is correctly fomated
    if line.find("--") > 0 and unique_names.regex.match(line):
        full_name = unique_names.regex.match(line).group()
        arr_name = full_name.split(", ")

        if full_name not in unique_names.full_name:
            unique_names.full_name.append(full_name)
        if arr_name[0] not in unique_names.last_name:
            unique_names.last_name.append(arr_name[0])
        if arr_name[1] not in  unique_names.first_name:
            unique_names.first_name.append(arr_name[1])
        

def search_for_unique_names_nbr(file_name):
    # Regular expression used to check if names are correctly formated
    unique_names.regex = re.compile("[a-zA-Z]+, [a-zA-Z]+")
    unique_names.full_name = []
    unique_names.first_name = []
    unique_names.last_name = []
    try:
        # Open and read line by line the file
        with open(file_name, 'r') as fd:
            for line in fd:
                unique_names(line)
    except IOError:
        print("Cannot open the file: " + file_name, file=sys.stderr)
        usage()
    else:
        print("There are ", len(unique_names.full_name), " unique full names.\n\
There are ", len(unique_names.first_name), " unique first names.\n\
There are ", len(unique_names.last_name), " unique last names.")
        fd.close()

def main():
    # Check if parameters seams ok
    if len(sys.argv) != 2:
        usage()
    search_for_unique_names_nbr(sys.argv[1])

if __name__ == "__main__":
    main()