# oledcomm-coding-exercise

### File architecture
.\
├── assets\
│   ├── test-data-10-exp-5.list\
│   └── test.txt\
├── coding-exercise-instructions.txt\
├── README.md\
└── src\
    ├── question1\
    │   ├── main.py\
    │   └── output.txt\
    ├── question2\
    │   ├── main.py\
    │   └── output.txt\
    ├── question3\
    │   ├── main.py\
    │   └── output.txt\
    └── question4\
        ├── main.py\
        └── output.txt

There are one subdirectory for each question of the test in the src directory.
In each question subdirectory there a main.py and an output.txt files. You can find some test files in the assets directory.

### Execution

To exection a program you have to go in the question directory and run

`$> ./main.py [FILE NAME|PATH]`\
or\
`$> python main.py [FILE NAME|PATH]`

For example:
`$> ./main.py ../../assets/test-data-10-exp-5.list`